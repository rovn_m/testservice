package edu.bigdata.training.testservice.service;

import edu.bigdata.training.testservice.config.ServiceConf;
import edu.bigdata.training.testservice.controller.model.Person;
import edu.bigdata.training.testservice.model.PersonEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ServiceConf.class)
public class TestBusinessLogicServiceTest {

    @Autowired
    private TestBusinessLogicService testBusinessLogicService;

    @Test
    public void testGetAll(){
        Person person = new Person("test");

        PersonEntity personEntity = testBusinessLogicService.processCreate(person);
        Assert.assertEquals(person.getName(), personEntity.getName());
        Assert.assertEquals(personEntity, testBusinessLogicService.processGetAll().get(0));
        Assert.assertEquals(person.getName(), testBusinessLogicService.processGet(personEntity.getId().toString()).getName());
    }

}
